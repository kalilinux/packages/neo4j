#!/bin/sh

set -e

if ! test $(id -u) -eq 0; then
	echo "ERROR: Tests must be run as root" >&2
	exit 1
fi

if ! neo4j start; then
	echo "ERROR: Failed to start the server" >&2
	exit 1
fi

# Running neo4j status right now succeeds, but maybe it's only
# because the server didn't crash yet! So let's wait a bit.
sleep 10s

if ! neo4j status | grep -q "^Neo4j is running"; then
	echo "ERROR: The server is not running" >&2
	exit 1
fi

if ! neo4j stop; then
	echo "ERROR: Failed to stop the server" >&2
	exit 1
fi
